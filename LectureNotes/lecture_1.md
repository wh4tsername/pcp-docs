# Лекция 1

## Введение

![](Figures/IntelPentium4_800px.jpg)
*Снимок кристалла процессора Intel Pentium 4. Источник [Fritzchens Fritz на Flickr](https://www.flickr.com/photos/130561288@N04/32847415126/).*

![](Figures/ScalingVsDensity_800px.png)
*[Источник IEEE Spectrum](https://spectrum.ieee.org/transistor-density)*

![](Figures/MooresLaw_800px.png)
*Зависимость количества транзисторов на микропроцессоре от года. Источник [Max Roser, Hannah Ritchie на ourworldindata.org](https://ourworldindata.org/uploads/2020/11/Transistor-Count-over-time.png)*

## Параллельность

### Закон Амдала

![](Figures/AmdahlsLaw_800px.png)
*[Источник Daniels220 из английской Википедии](https://en.wikipedia.org/wiki/Amdahl%27s_law)*

Ускорение можно рассчитать по формуле:

$$
S_p=1/\left[\alpha+\frac{1-\alpha}{p}\right]
$$

где $\alpha$-доля параллелизуемого кода, $p$-число вычислителей.

## Конкурентность

## Поток


## Страничная организация памяти

![](Figures/MemoryPageDirectory_800px.png)
*Страничная организация памяти.*

## Ссылки:

[Stack frame layout on x86-64 - Eli Bendersky's website](https://eli.thegreenplace.net/2011/09/06/stack-frame-layout-on-x86-64)

[Paging - wiki.osdev.org](https://wiki.osdev.org/Paging)

[Raw Linux Threads via System Calls - Chris Wellons website](https://nullprogram.com/blog/2015/05/15/)