# Лекция 2


## Переключение потоков

![](Figures/ThreadsAndFiberSwitching_800px.png)
*Переключение потоков.*

## Атомарные переменные

##  Data race

## Race condition


## Пример: Односвязный список

```cpp
Node {
    int v;
    Node* next = nullptr;
};

push(int v)
{
    node = new Node(v);
    node->next = head_;
    head_ = node;
}
```

#IMG Как ломается push()

Последние две команды в коде push() - секция, в которую хочется пускать только один поток. Эта часть кода называется критической секции. Мы хотим там добиться взаимного исключения (MUTual EXclusion). Реализуется это с помощью примитива mutex.

### Mutex

В начале и конце критической секции ставим два вызова:


```cpp
push(int v)
{
    node = new Node(v);
    m.lock();
    node->next = head_;
    head_ = node;
    m.unlock();
}
```

От mutex мы хотим две вещи:
1. Взаимное исключение: только один поток может находится между lock() и unlock().
2. Liveness когда разные потоки вызывают lock(), хотя бы один из вызовов должен вернуться успехом. Т.е. mutex должен давать возможность критической секции кода исполняться. Все потоки должны в какой-то момент прогрессировать.


### Порядок исполнения

```cpp
void runMeManyTimes()
{

    int x = 0;
    int y = 0;

    int r1 = 0;
    int r2 = 0;

    std::thread t1([&]() {
        x = 1;
        r1 = y;
    });

    std::thread t2([&]() {
        y = 1;
        r2 = x;
    });

    t1.join();
    t2.join();

    if (r1 == 0 && r2 == 0)
    {
        std::cout << "Something is not in order: ";
        std::cout << "r1 = " << r1 << ", r2 =  " << r2 << std::endl;
    }
}
```

![](Figures/UnorderedExecution_800px.png)
*Порядок исполнения команд не всегда очевиден. Из кода кажется, что хотя бы одна переменная - r1 или r2 должна быть равна единице. Но если выполнить код много раз, то эпизодически можно наблюдать ситуацию когда r1=r2=0. [нужна ссылка]*

## Cсылки по лекции

https://en.cppreference.com/w/cpp/atomic/atomic

https://en.cppreference.com/w/cpp/thread/unique_lock

https://en.cppreference.com/w/cpp/thread/lock_guard

## Для повторения акоса и людям, у которых не было плюсов

https://eli.thegreenplace.net/2011/02/04/where-the-top-of-the-stack-is-on-x86/

https://eli.thegreenplace.net/2011/09/06/stack-frame-layout-on-x86-64