project = 'PCP'
extensions = ['myst_parser']
exclude_patterns = ['_build']
html_theme = 'alabaster'
html_static_path = ['_static']
